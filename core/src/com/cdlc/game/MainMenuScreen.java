package com.cdlc.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by sduignan on 14/02/2017.
 */

public class MainMenuScreen implements Screen {
    final CremeDeLaCreme game;

    private Stage stage;
    private TextButton button;
    private TextButton.TextButtonStyle buttonStyle;

    private BitmapFont font;

    public MainMenuScreen(final CremeDeLaCreme _game){
        this.game = _game;
        stage = new Stage(game.viewport);

        font = new BitmapFont();
        buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = font;
        button = new TextButton("Gamplay", buttonStyle);
        button.setWidth(game.SCREEN_WIDTH/3);
        button.setHeight(game.SCREEN_HEIGHT/4);

        button.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("my app", "Pressed"); //** Usually used to start Game, etc. **//

                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("my app", "Released");
                game.setScreen(new GamePlayScreen(game));
                dispose();
            }
        });

        button.setPosition(game.SCREEN_WIDTH/2, game.SCREEN_HEIGHT/2);
        stage.addActor(button);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        game.batch.setProjectionMatrix(game.camera.combined);
        game.batch.begin();
        //game.batch.draw(game.img, 0, 0);
        stage.draw();
        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
